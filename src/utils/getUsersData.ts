import axios from "axios";
import { User } from "../store/state";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore: Implicitly has any type
export default async function getUsers(): Promise<User[] | null> {
  await axios
    .get("https://api.github.com/users?per_page=10")
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
      return null;
    });
}
